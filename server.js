// Require express
const express = require('express');
// express app
const app = express();

// listen for requests
app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'));

// Allow static files
app.use(express.static(__dirname + '/public'));
// register view engine
app.set('view engine', 'ejs');
// app.set('views', 'myviews');

app.get('/', (req, res) => {
  res.render('pages/index');
});

// 404 page
// app.use((req, res) => {
//   res.status(404).render('pages/404', { title: '404' });
// });

