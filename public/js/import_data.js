/* 
Data Sources:
| Hoogspanningskabels: https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wfs?request=GetFeature&service=WFS&version=1.1.0&typeName=hoogspanningskabels&outputFormat=application%2Fjson%3B%20subtype%3Dgeojson
| Middenspanningskabels: https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wfs?request=GetFeature&service=WFS&version=1.1.0&typeName=middenspanningkabels&outputFormat=application%2Fjson%3B%20subtype%3Dgeojson
| Laagspanningkabels: https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wfs?request=GetFeature&service=WFS&version=1.1.0&typeName=laagspanningkabels&outputFormat=application%2Fjson%3B%20subtype%3Dgeojson
| Laagspanningsverdeelkasten: https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wfs?request=GetFeature&service=WFS&version=1.1.0&typeName=laagspanningsverdeelkasten&outputFormat=application%2Fjson%3B%20subtype%3Dgeojson
| Middenspanningsinstallaties: https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wfs?request=GetFeature&service=WFS&version=1.1.0&typeName=middenspanningsinstallaties&outputFormat=application%2Fjson%3B%20subtype%3Dgeojson
| Onderstations: https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wfs?request=GetFeature&service=WFS&version=1.1.0&typeName=onderstations&outputFormat=application%2Fjson%3B%20subtype%3Dgeojson

| Windturbines NL: https://www.nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search#/metadata/90f5eab6-9cea-4869-a031-2a228fb82fea
| Windenergie data per regio: https://www.cbs.nl/nl-nl/cijfers/detail/70960ned

| Atlas leefomgeving rivm - Hoogspanningsnet: https://www.nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search#/metadata/1261ea0d-a5bb-4144-a796-c5b743593f57?tab=general
*/

let projectdata = null;

// Load all project data
async function loadProjectData() {
    try {
        const data1_l_hsk = await f_loadJsonDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_1_l_hsk}`);    // File Contains: Hoogspanningskabels
        const data2_l_lsk = await f_loadJsonDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_2_l_lsk}`);    // File Contains: Laagspanningkabels
        const data3_l_lsvk = await f_loadJsonDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_3_l_lsvk}`);  // File Contains: Laagspanningsverdeelkasten
        const data4_l_msk = await f_loadJsonDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_4_l_msk}`);    // File Contains: Middenspanningskabels
        const data5_l_msi = await f_loadJsonDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_5_l_msi}`);    // File Contains: Middenspanningsinstallaties
        const data6_l_os = await f_loadJsonDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_6_l_os}`);      // File Contains: Onderstations
        const data7_nl_wt = await f_loadJsonDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_7_nl_wt}`);    // File Contains: Windturbines locatie NL
        // const data8_alr_hs = await f_loadJsonDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_8_alr_hs}`);    // File Contains: Hoogspanningsnet: Atlas leefomgeving rivm

        await dataToLineStrings(data1_l_hsk, `#32a852`, 1); // Linestring
        await dataToLineStrings(data2_l_lsk, `#8532a8`, 1); // Linestring
        await dataToPoints(data3_l_lsvk, `#a83258`, 3); // Point
        await dataToLineStrings(data4_l_msk, `#a86432`, 1); // Linestring
        await dataToPolygons(data5_l_msi, `#fcba03`, `#FFF`); // Polygons
        await dataToPoints(data6_l_os, `#323ea8`, 3); // Point
        await dataToMultiPoints(data7_nl_wt, `#8f8a7c`, 3); // MultiPoint
        // await dataToMultiPoints(data8_alr_hs, `#000`, 3); // MultiLineString

        // Select shift
        // f_allow_group_select_checkboxes(`mapList`);

        // Load NL wind energy data : const subject_data1_nlwtp
        DATA_WINDENERGY_NL = await f_loadCsvDataFromFile(`${DATAMAP_LOCATION}/${FILENAME_SUBJECT_1_nlwtp}`);  // File Contains: Windturbines NL production per region

    } catch(error) {
        console.log(error);
    }
}

loadProjectData();

