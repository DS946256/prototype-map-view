mapboxgl.accessToken = MAPBOX_TOKEN;
const map = new mapboxgl.Map({
    container: 'map', // container ID
    style: 'mapbox://styles/mapbox/streets-v11', // style URL
    center: [NL_MAP_CENTER_LOT, NL_MAP_CENTER_LAT], // starting position
    zoom: NL_MAP_ZOOM // starting zoom
});

// Vislualize Linestring
function mp_visualizeLineString(data, id, lineColor = `#000`, lineWidth = 5) {        

    map.on('load', () => {
        map.addSource(id, {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': data
            }
        });

        map.addLayer({
            'id': id,
            'type': 'line',
            'source': id,
            'layout': {
                'visibility': 'none',
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': lineColor,
                'line-width': lineWidth
            }
        });
    });        
}

// Vislualize Polyogon
function mp_visualizePolygons(data, id, fillColor = `#000`, borderColor = `#000`) {

    // console.log(id);

    map.on('load', () => {
        // Add a data source containing GeoJSON data.
        map.addSource(id, {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': data
            }
        });
    
        // Add a new layer to visualize the polygon.
        map.addLayer({
            'id': id,
            'type': 'fill', 
            'source': id,
            'layout': {                
                'visibility': 'none',
            },
            'paint': {
                'fill-color': fillColor, 
                'fill-opacity': 1
            }
        });

    });
}

// Vislualize Points
function mp_visualizePoints(data, id, circleColor = `#000`, circleSize = 5) {

    map.on('load', () => {
        map.addSource(id, {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': data
            }
        });

        map.addLayer({
            'id': id,
            'type': 'circle',
            'source': id,
            'layout': {                
                'visibility': 'none',
            },
            'paint': {
                'circle-color': circleColor,                
                'circle-radius': circleSize,                
            }
        });
    });   
}

// Vislualize MultiPoints
function mp_visualizeMultiPoints(data, id, circleColor = `#000`, circleSize = 5) {

    map.on('load', () => {
        map.addSource(id, {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': data
            }
        });

        map.addLayer({
            'id': id,
            'type': 'circle',
            'source': id,
            'layout': {                
                'visibility': 'none',
            },
            'paint': {
                'circle-color': circleColor,                
                'circle-radius': circleSize,                
            }
        });
    });   
}

// Hide or show layer
function mp_hideLayer(id) {

    const visibility = map.getLayoutProperty(
        id,
        'visibility'
    );

    // Toggle layer visibility by changing the layout object's visibility property.
    if (visibility === 'visible') {
        map.setLayoutProperty(id, 'visibility', 'none');
        this.className = '';
    } else {
        this.className = 'active';
        map.setLayoutProperty(
            id,
            'visibility',
            'visible'
        );
    }
}

// Place a dialog on coordinate
function mp_placeDialog(coords = null, ide = f_genRandomId(), title = null, txt = null) {

    const monument = [-77.0353, 38.8895];

    const popup = new mapboxgl.Popup({ offset: 25 }).setHTML(
        `<h2>${title}</h2><p>${txt}</p>`
    );
         
    // create DOM element for the marker
    const el = document.createElement('div');
    el.className = 'marker';
    el.id = ide;
        
    // create the marker
    new mapboxgl.Marker(el)
        .setLngLat(coords)
        .setPopup(popup) // sets a popup on this marker
        .addTo(map); 

    popup.on('open', () => {
        console.log('popup was opened');
    });        
}

// Go to coordinates
function mp_goToCoord(aFeature, zoom = 8) {
    map.flyTo({
        center: aFeature.geometry.coordinates,
        zoom: zoom
    });
}

// Get geocode forward
async function mp_getGeoCodeForward(searchStr, types, limit = 1) {
    return await f_sendRequestGet(`https://api.mapbox.com/geocoding/v5/mapbox.places/${searchStr}.json?types=${types}&limit=${limit}&access_token=${MAPBOX_TOKEN}`);    
}

// Get geocode reverse

// map.on('load', () => {
//     // Add a data source containing GeoJSON data.
//     map.addSource('maine', {
//         'type': 'geojson',
//         'data': {
//             'type': 'Feature',
//             'geometry': {
//                 'type': 'Polygon',
//                 // These coordinates outline Maine.
//                 'coordinates': [
//                     [
//                         [-67.13734, 45.13745],
//                         [-66.96466, 44.8097],
//                         [-68.03252, 44.3252],
//                         [-69.06, 43.98],
//                         [-70.11617, 43.68405],
//                         [-70.64573, 43.09008],
//                         [-70.75102, 43.08003],
//                         [-70.79761, 43.21973],
//                         [-70.98176, 43.36789],
//                         [-70.94416, 43.46633],
//                         [-71.08482, 45.30524],
//                         [-70.66002, 45.46022],
//                         [-70.30495, 45.91479],
//                         [-70.00014, 46.69317],
//                         [-69.23708, 47.44777],
//                         [-68.90478, 47.18479],
//                         [-68.2343, 47.35462],
//                         [-67.79035, 47.06624],
//                         [-67.79141, 45.70258],
//                         [-67.13734, 45.13745]
//                     ]
//                 ]
//             }
//         }
//     });

//     // Add a new layer to visualize the polygon.
//     map.addLayer({
//         'id': 'maine', // = ID for reference
//         'type': 'fill',
//         'source': 'maine', // reference the data source
//         'layout': {},
//         'paint': {
//             'fill-color': '#0080ff', // blue color fill
//             'fill-opacity': 0.5
//         }
//     });
//     // Add a black outline around the polygon.
//     map.addLayer({
//         'id': 'outline',
//         'type': 'line',
//         'source': 'maine',
//         'layout': {},
//         'paint': {
//             'line-color': '#000',
//             'line-width': 3
//         }
//     });
// });

// [
//     [52.51100318578179, 5.07690621873576],
//     [52.511007679376874, 5.076906187178345],
//     [52.511007706332116, 5.076916498178974],
//     [52.51100321273705, 5.076916529735339],
//     [52.51100318578179, 5.07690621873576]
// ]

    // [
    //     [-67.13734, 45.13745],
    //     [-66.96466, 44.8097],
    //     [-68.03252, 44.3252],
    //     [-69.06, 43.98],
    //     [-70.11617, 43.68405],
    //     [-70.64573, 43.09008],
    //     [-70.75102, 43.08003],
    //     [-70.79761, 43.21973],
    //     [-70.98176, 43.36789],
    //     [-70.94416, 43.46633],
    //     [-71.08482, 45.30524],
    //     [-70.66002, 45.46022],
    //     [-70.30495, 45.91479],
    //     [-70.00014, 46.69317],
    //     [-69.23708, 47.44777],
    //     [-68.90478, 47.18479],
    //     [-68.2343, 47.35462],
    //     [-67.79035, 47.06624],
    //     [-67.79141, 45.70258],
    //     [-67.13734, 45.13745]
    // ]

    // map.on('load', () => {

    //     map.addSource(randId, {
    //         'type': 'geojson',
    //         'data': {
    //             'type': 'Feature',
    //             'properties': {},
    //             'geometry': {
    //                 'type': 'LineString',
    //                 'coordinates': 
    //                     data                    
    //             }
    //         }
    //     });
    //     map.addLayer({
    //         'id': randId,
    //         'type': 'line',
    //         'source': randId,
    //         'layout': {
    //             'line-join': 'round',
    //             'line-cap': 'round'
    //         },
    //         'paint': {
    //             'line-color': '#ff0000',
    //             'line-width': 8
    //         }
    //     });
    // });


    // map.on('load', () => {
    //     // Add an image to use as a custom marker
    //     map.loadImage(
    //         'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
    //         (error, image) => {
    //             if (error) throw error;
    //             map.addImage('custom-marker', image);
    //             // Add a GeoJSON source with 2 points
    //             map.addSource(randId, {
    //                 'type': 'geojson',
    //                 'data': {
    //                     'type': 'FeatureCollection',
    //                     'features': data
    //                 }
    //             });

    //             // Add a symbol layer
    //             map.addLayer({
    //                 'id': randId,
    //                 'type': 'symbol',
    //                 'source': randId,
    //                 'layout': {
    //                     'icon-image': 'custom-marker',
    //                     // get the title name from the source's "title" property
    //                     'text-field': ['get', 'title'],
    //                     'text-font': [
    //                         'Open Sans Semibold',
    //                         'Arial Unicode MS Bold'
    //                     ],
    //                     'text-offset': [0, 1.25],
    //                     'text-anchor': 'top'
    //                 }
    //             });
    //         }
    //     );
    // }); 