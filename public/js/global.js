// ------------------------------------------------------------------------------------------------------ //
// Variables

const DATAMAP_LOCATION = `./data`;

// OLD lat/lot = -68.137343, 45.137451
const NL_MAP_CENTER_LAT = `52.0822603`;
const NL_MAP_CENTER_LOT = `5.3924617`;
const NL_MAP_ZOOM = `6.1`;

// Map layer data
const FILENAME_1_l_hsk = `leander_hoogspanningskabels.geojson`;
const FILENAME_2_l_lsk = `leander_laagspanningkabels.geojson`;
const FILENAME_3_l_lsvk = `leander_laagspanningsverdeelkasten.geojson`;
const FILENAME_4_l_msk = `leander_middenspanningkabels.geojson`;
const FILENAME_5_l_msi = `leander_middenspanningsinstallaties.geojson`;
const FILENAME_6_l_os = `leander_onderstations.geojson`;
const FILENAME_7_nl_wt = `windturbines_nl.geojson`;
const FILENAME_8_alr_hs = `atlasleefomgeving_hoogspanningsnet.geojson`;

// Subject file list
const FILENAME_SUBJECT_1_nlwtp = `windenergie_opwekking_regios_2020.csv`;

// Variable containing Wind energy data from csv file
let DATA_WINDENERGY_NL = null;

// Mapbox token
const MAPBOX_TOKEN = "pk.eyJ1IjoiZHMyNDEyIiwiYSI6ImNqcGd6MGthbzByM2wzeG83cDFlMGE5eHUifQ.J7swoaxJRo-dvwcjZyWmzA";

// ------------------------------------------------------------------------------------------------------ //
// Functions

// Load data from files
async function f_loadJsonDataFromFile(location) {
    return await fetch(location)
    .then((response) => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error('Something went wrong');
        }
    })
    .catch((error) => {
        console.log(error);
        return null;
    });
}

async function f_loadCsvDataFromFile(location) {
    // Get CSV
    var request = new XMLHttpRequest();  
    request.open("GET", location, false);   
    request.send(null);  

    // Return res    
    return f_returnedCsvArrToArrOfObj(f_csvStringToArray(request.responseText));
}

// CSV string to JS array
const f_csvStringToArray = strData =>
{
    const objPattern = new RegExp(("(\\,|\\r?\\n|\\r|^)(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|([^\\,\\r\\n]*))"),"gi");
    let arrMatches = null, arrData = [[]];
    while (arrMatches = objPattern.exec(strData)){
        if (arrMatches[1].length && arrMatches[1] !== ",")arrData.push([]);
        arrData[arrData.length - 1].push(arrMatches[2] ? 
            arrMatches[2].replace(new RegExp( "\"\"", "g" ), "\"") :
            arrMatches[3]);
    }
    return arrData;
}

// CSV returned array to array of objects with keys
function f_returnedCsvArrToArrOfObj(input) {
    // Empty return arr
    let returnArr = []

    // 1 row = keys
    const properties = input[0]

    // loop through all array items
    for(var i = 1; i < input.length; i++) {

        let obj = {};

        // Loop through the properties(headers), and add to array
        for(var j = 0; j < properties.length; j++) {

            // Add each object to arr
            obj[properties[j]] = input[i][j];
        }

        // Add to return arr
        returnArr.push(obj);

        // Set obj into null
        obj = null;
    }
    // Return filled array
    return returnArr;
}

// View Object Properties
function f_viewObjectProperties(obj) {    
    return Object.keys(obj);
}

// Generate Random ID
function f_genRandomId() {
    var S4 = function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
     };
     return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

// Convert numeric
function f_stringToNumeric(str) {
    if (typeof str != "string") { 
        return false // we only process strings!  
    } else {
        if (!isNaN(str) && !isNaN(parseFloat(str)) ) {
            return parseInt(str);
        } else {
            return null;
        }
    }
     
  }

// TODO: Have option to NOT ACCEPT null value results IN comparisons
// Get highest value of propertie of array 
function f_getObjResultOfCaseInArray(arr, propertyName, resultIs = "HighestValue", doNotAcceptNullValues = false) {
    let currentBest = null; // = 1 object of the arr

    if (resultIs == `HighestValue`) {
        // Loop through arr
        for(var i = 0; i < arr.length; i++) {
            // Search On property
            // console.log(arr[i][`${propertyName}`]);
            // console.log(arr);
            // Convert to number
            let num = f_stringToNumeric(arr[i][`${propertyName}`]);

            
            // If is higher > set  
            if (currentBest == null) {
                currentBest = arr[i];
            } else if (num > currentBest[`${propertyName}`]) {
                currentBest = arr[i];
            }
        }

        // Return result
        return currentBest;

    } else if (resultIs == `LowestValue`) {

        // Loop through arr
        for(var i = 0; i < arr.length; i++) {
            // Search On property

            // Convert to number
            let num = f_stringToNumeric(arr[i][`${propertyName}`]);

            // If is higher > set  
            if (currentBest == null) {
                currentBest = arr[i];
            } else if (num < currentBest[`${propertyName}`]) {
                currentBest = arr[i];
            }
        }

        return currentBest;
    } else {
        return null;
    }
}

// Add Map option in HTML
function f_addMapToList(mapName, mapId) {
    // Get Element
    let elem = document.getElementById('mapList');

    // Create HTML element
    // let toAddHtml = `<p class="layerTxt">${mapName} </p>`;
    let toAddHtml = `<div class="row">
                        <div class="col-sm-12 col-lg-10">
                            <p class="layerTxt">${mapName}</p>
                        </div>
                        <div class="col-sm-12 col-lg-2">
                            <input class="layerHide" type="checkbox" onclick="mp_hideLayer('${mapId}')">
                        </div>
                    </div>`;

    // Add to end of html
    elem.innerHTML += toAddHtml;
}

// Show hide subjects
function f_showSubject() {
    
    // Get select input
    let selectInput = document.getElementById("subjectSelect");

    // Hide all first
    document.getElementById("subjectWindNL").style.display = "none";
    document.getElementById("subjectUnknown").style.display = "none";

    // Get selected item > show selected item
    let selected = selectInput.value;    

    if (selected == "1") {
        document.getElementById("subjectWindNL").style.display = "block";
        
    } else if (selected == "2") {
        document.getElementById("subjectUnknown").style.display = "block";
    }
    
}

// Send request
async function f_sendRequestGet(url) {
    var opts = {
        method: 'GET',      
        headers: {}
    };

    return await fetch(url, opts)
    .then((response) => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error('Something went wrong');
        }
    })
    .catch((error) => {
        console.log(error);
        return null;
    });
}

// Shift select checkboxes
function f_allow_group_select_checkboxes(checkbox_wrapper_id){
    console.log(checkbox_wrapper_id);
    var lastChecked = null;
    var checkboxes = document.querySelectorAll('#'+checkbox_wrapper_id+' input[type="checkbox"]');

    //I'm attaching an index attribute because it's easy, but you could do this other ways...
    for (var i=0;i<checkboxes.length;i++){
        checkboxes[i].setAttribute('data-index',i);
    }

    for (var i=0;i<checkboxes.length;i++){
        checkboxes[i].addEventListener("click",function(e){

            if(lastChecked && e.shiftKey) {
                var i = parseInt(lastChecked.getAttribute('data-index'));
                var j = parseInt(this.getAttribute('data-index'));
                var check_or_uncheck = this.checked;

                var low = i; var high=j;
                if (i>j){
                    var low = j; var high=i; 
                }

                for(var c=0;c<checkboxes.length;c++){
                    if (low <= c && c <=high){
                        checkboxes[c].checked = check_or_uncheck;
                    }   
                }
            } 
            lastChecked = this;
        });
    }
}

// async function loadDataFromFile(location) {
//     const response = await fetch(location)
//     .then(response => response.json())
//     .then(data => {
//             console.log(data)
//             console.log(Object.keys(data));
//         });  
// }