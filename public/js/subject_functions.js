// Function for analyzing Windenergy in NL only
async function s_getAndSetHighOrLowValuePointDialogFromArrToMap(propertyName, resultIs) {
    // Get result first
    let result = f_getObjResultOfCaseInArray(DATA_WINDENERGY_NL, propertyName, resultIs);

    // First get GeoCode
    let geocodeRes = await mp_getGeoCodeForward(result.regios, "region");

    // Create dialog text
    let dialogTxt = '';

    for (const property in result) {        
        dialogTxt += `<b>${property}</b>: ${result[property]}<br>`;
     }

    // Prepare data > Set dialog and point    
    mp_placeDialog(geocodeRes.features[0].geometry.coordinates,null,result.regios, dialogTxt);

    // Zoom to the location
    mp_goToCoord(geocodeRes.features[0]);
}