// Get data > convert to correct format for mapbox > use visual functions
// Abstraction on Map type / map settings when visualizing the data
// Searching through data > fastest way: analyze fastest method, also ask others for opinions.
// PROBLEM: Each data source has different property structure. TYPE OF STRUCTURE or settings equivelant is required for correct conversion.

async function dataToPolygons(input, fillColor = null, borderColor = null) {
    // Get random id
    const randId = f_genRandomId();    
    // Create actual id
    const actualId = `${input.name}${randId}`;  
    // Visualize data in map  
    mp_visualizePolygons(input.features, actualId, fillColor, borderColor);
    // Add to Map List
    f_addMapToList(input.title, actualId);
}

async function dataToPoints(input, circleColor = null, circleSize = null) {
    // Get random id
    const randId = f_genRandomId();    
    // Create actual id
    const actualId = `${input.name}${randId}`;
    // Visualize data in map
    mp_visualizePoints(input.features, actualId, circleColor, circleSize);
    // Add to Map List
    f_addMapToList(input.title, actualId);
}

async function dataToLineStrings(input, lineColor = null, lineWidth = null) {

    // Get random id
    const randId = f_genRandomId();    
    // Create actual id
    const actualId = `${input.name}${randId}`;
    // Visualize data in map
    mp_visualizeLineString(input.features, actualId, lineColor, lineWidth);
    // Add to Map List
    f_addMapToList(input.title, actualId);
}

async function dataToMultiPoints(input, circleColor = null, circleSize = null) {
    // Get random id
    const randId = f_genRandomId();    
    // Create actual id
    const actualId = `${input.name}${randId}`;
    // Visualize data in map
    mp_visualizeMultiPoints(input.features, actualId, circleColor, circleSize);
    // Add to Map List
    f_addMapToList(input.title, actualId);
}

// coordinates = [
//         [ 
//             [ 5.07690621873576, 52.511003185781789 ], [ 5.076906187178345, 52.511007679376874 ], [ 5.076916498178974, 52.511007706332116 ], [ 5.076916529735339, 52.511003212737052 ], [ 5.07690621873576, 52.511003185781789 ]
//         ] 
//     ];

//     "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::28992" } },

    // console.log(f_viewObjectProperties(input.features));
    // input.features[0].geometry.coordinates[0][0]
    // console.log(input.features[0].geometry.coordinates); // Coordinate array = THIS IS 1 POLYGON

    // visualizePolygons(input.features[0].geometry.coordinates[0]);
    // visualizePolygons(input.features[0].geometry.coordinates[1]);
    // visualizePolygons(input.features[0].geometry.coordinates[2]);
    // visualizePolygons(input.features[0].geometry.coordinates[3]);
    // visualizePolygons(input.features[0].geometry.coordinates[4]);

    // var max = 15;
    // var count = 1;

    // visualizeLineString(input.features);

    // Loop through: FEATURES
    // input.features.forEach(element => {
    //     if (count < max) {
    //         // visualizePolygons(element.geometry.coordinates[0]);
    //         visualizeLineString(element.geometry.coordinates);
    //         count++;
    //     }
    // });

    // > Output polygons